package com.pack.SpringBootExternalTomcat.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.pack.SpringBootExternalTomcat.model.User;

public interface UserRepository extends JpaRepository<User, Integer>
{

}