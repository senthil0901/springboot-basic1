package com.pack.SpringBootExternalTomcat;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;

@SpringBootApplication
public class SpringBootExternalTomcatApplication extends SpringBootServletInitializer{

    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
        return application.sources(SpringBootExternalTomcatApplication.class);
    }
 

	public static void main(String[] args) {
		SpringApplication.run(SpringBootExternalTomcatApplication.class, args);
	}

}

//https://www.javaguides.net/2018/09/spring-boot-deploy-war-file-to-external-tomcat.html