package com.pack.SpringBootJPA1;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

public interface EmployeeRepository extends CrudRepository<Employee,Integer> {
	  //Derived Query
	  List<Employee> findByDept(String deptName);
	  List<Employee> findBySalaryGreaterThan(double salary);
	  List<Employee> findByDeptAndSalaryLessThan(String deptName, double salary);
	  
	  //Limited Query
	  Employee findTopByOrderBySalaryDesc();
	  Employee findTopByOrderBySalaryAsc();
	  List<Employee> findTop3ByOrderBySalaryDesc();
	  List<Employee> findTop3ByOrderBySalaryAsc();
	  List<Employee> findFirst2BySalary(double salary);
	  List<Employee> findFirst2ByDeptOrderBySalaryDesc(String deptName);
	  
	  //LIKE Expression
	  List<Employee> findByNameLike(String likeString);
	  List<Employee> findByDeptLike(String likeString);
	  
	  //count query
	  long countByDept(String deptName);
	  long countBySalaryGreaterThanEqual(double salary);
	  long countByNameEndingWith(String endString);
	  long countByNameLike(String likeString);
}
